#igrca.py

#player position
x = 100
y = 100

#counter for key presses
presscnt = 0

#milliseconds since begging of program
dedmillis = 0

#size of player and dots
offset = 10

#boolean for the alive state of player
ded = False

#framerate
frames = 60

#clears appdata
reset = True

#reads death value from file score.txt
reader = createReader("score.txt")
dedcount = int(reader.readLine())


#clears background and dots
def clear(par):
    loadPixels()
    for i in range((width*height)): 
        pixels[i] = par
    updatePixels()   

#text setting for out of bounds
def txtset():
    textAlign(CENTER,CENTER)
    textSize(30)

#define function resetplayer
def resetplayer():
    global dedcount
    global presscnt
    global x,y
    global dedmillis
    x = 100
    y = 100
    presscnt = 0
    dedcount += 1
    dedmillis = millis()

#basic setup values
def setup():
    global x,y
    global background
    global offset
    global frames
    global timeout

    
    size(600,600)
    frameRate(frames)
    noStroke()
    background(200)

#players tail and when he dies
def tail():
    global presscnt
    global ded
    global dedcount
    global dedmillis

    if(get(x,y) == -14145496):

        #width changes color depending on screen size
        clear(width)
        
        #checks for millisecond difference
        dedmillis = millis()
        #writes out died and text values
        #diffrent text color is a feature
        fill(40)
        died = "You died after " + str(presscnt) + " presses"
        textAlign(CENTER,CENTER)
        textSize(30)
        text(died,width/2,height/2)
        presscnt = 0
        ded = True
        dedcount += 1
        

       
       


def start():
    global x,y
    global background
    global offset
    global frames
    global reset
    global presscnt

    #difficulty selection
    if (reset == True and keyCode == 49):
        offset = 10
        frames = 8


    if (reset == True and keyCode == 50):
        offset = 10
        frames = 4

    
    if (reset == True and keyCode == 51):
        offset = 10
        frames = 1

    reset = False
    clear(color(200))
    presscnt = 0
    x = 100
    y = 100




#writes input to variable key
def keyPressed():
    global presscnt
    global x,y
    global ded
    global reset
    global dedcount

    if (reset == False):
        #checks the collor of pixel
        loadPixels()
        get(x,y)

        #check if key is pressed
        if (keyPressed):
            
            #counts key presses
            if (ded == False):
                presscnt += 1

            #color old location
            fill(40)
            ellipse(x,y,offset/2,offset/2)
            
            #basic movement for w
            if (ded == False and keyCode == 87):   
                y -= offset
                tail()

            #basic movement for d
            elif (ded == False and keyCode == 68):     
                x += offset
                tail()

            #basic movement for s
            elif (ded == False and keyCode == 83):          
                y += offset
                tail() 

            #basic movement for a
            elif (ded == False and keyCode == 65):           
                x -= offset
                tail()
            
            #enter gets a purpose
            elif (ded == True and keyCode == 10):
                ded = False
                reset = True
                clear(color(200))
 
        else:
            return()
    
    else:
        #start needs to be called here for the diffculty selection
        start()


def draw():
    global x,y
    global presscnt
    global ded
    global reset
    global dedcount
    global dedmillis
    global frames

    if (reset == False):
        
      
        fill(150)
        ellipse(x,y,offset/2,offset/2)

        if(ded != True and random(100) < 5*frames):
            fill(40)
            ellipse(random(width),random(height),offset,offset)

        #out of bounds on any side start
        if (x > width):
            txtset()
            clear(color(200))
            ded = True
            text("Out of bounds!",width/2,height/2)
            resetplayer()
        
        if (x < 0):
            txtset()
            clear(color(200))
            ded = True
            text("Out of bounds!",width/2,height/2)
            resetplayer()
    
        if (y > height):
            txtset()
            clear(color(200))
            ded = True
            text("Out of bounds!",width/2,height/2)
            resetplayer()
        
        if (y < 0):
            txtset()
            clear(color(200))
            ded = True
            text("Out of bounds!",width/2,height/2)
            resetplayer()
        #out of bounds on any side end
        
        #counter for presses
        fill(140)
        rect(0,570,50,30,0,55,0,0)
        rect(550,570,50,30,55,0,0,0)
        fill(40)
        textSize(14)
        textAlign(CENTER,CENTER)
        text(presscnt,30,585)
        text(dedcount,566,585)

        #loads image for deaths
        deadpic = loadImage("deadpic.png")
        image(deadpic,577,576)

        #loads image for presses
        clickpic = loadImage("clickpic.png")
        image(clickpic,5,576)

        #writes death valueto document score.txt
        output = createWriter("score.txt")
        output.print(dedcount)
        output.flush()
        output.close()

        #Tip if waiting on menu screen
        if (millis() - dedmillis > 5000):
            
            if (ded == True):
                textSize(20)
                text("Press ENTER to continue.",width/2,400)
            
          
    else:
        #difficulty selection text
        textAlign(CENTER,CENTER)
        textSize(20)
        text("Press 1-3 for the difficulty(3 being the easiest)",width/2,height/2)

